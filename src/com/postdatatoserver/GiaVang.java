package com.postdatatoserver;

import java.io.Serializable;

public class GiaVang implements Serializable {
	String id;
	String thanhPho;
	String mua;
	String ban;
	String loaiVang;
	public GiaVang(String id,String thanhPho,String loaiVang,String mua,String ban){
		this.id=id;
		this.thanhPho=thanhPho;
		this.mua=mua;
		this.ban=ban;
		this.loaiVang=loaiVang;
	}
	public String getId(){
		return this.id;
	}
	public String getThanhPho(){
		return this.thanhPho;
	}
	public String getLoaiVang(){
		return this.loaiVang;
	}
	public String getMua(){
		return this.mua;
	}
	public String getBan(){
		return this.ban;
	}
}
