package com.postdatatoserver;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class GiaVangAdapter extends ArrayAdapter<GiaVang> {
	Activity context;
	ArrayList<GiaVang> arr;
	int layout_id;
	public GiaVangAdapter(Activity context, int resource,ArrayList<GiaVang> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.layout_id=resource;
		this.arr=objects;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView=context.getLayoutInflater().inflate(layout_id, null);
		
		TextView tvTp=(TextView)convertView.findViewById(R.id.tvThanhPho);
		TextView tvLoaiVang=(TextView)convertView.findViewById(R.id.tvLoai);
		TextView tvMua=(TextView)convertView.findViewById(R.id.tvMua);
		TextView tvBan=(TextView)convertView.findViewById(R.id.tvBan);
		GiaVang g=arr.get(position);
		
		tvTp.setText(g.getThanhPho());
		tvLoaiVang.setText(g.getLoaiVang());
		tvMua.setText(g.getMua());
		tvBan.setText(g.getBan());
		return convertView;
	}
}
