package com.postdatatoserver;

import java.util.Date;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.logging.SimpleFormatter;




























import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.string;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.text.format.DateFormat;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	private String url="http://sentienich.aviostore.com/server_tienich/giavang/giavang.php";
	private static final String TAG_ID="id";
	private static final String TAG_MUA="mua";
	private static final String	TAG_BAN="ban";
	private static final String	TAG_THANHPHO="thanhpho";
	private static final String TAG_LOAIVANG="loaivang";
	public static final int SHOW_DATA=1;
	public static final int LOAD_DATA=2;
	int index;
	ArrayList<Date_GiaVang> arrDateGiaVang=new ArrayList<Date_GiaVang>();
	ArrayList<String>arrayDate=new ArrayList<String>();
	Date_GiaVang Dgv;
	//String d;
	ProgressDialog pDialog=null;
	TextView tvDate;
	Date date;
	Calendar cal;
	JSONArray arr=null;
	private int mYear;
	private int mDay;
	private int mMonth;
	int maxSize;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tvDate=(TextView)findViewById(R.id.tvDate);
		getCurrentDate();
		
		tvDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDatePicker(v);
			}
		});
		//senDateToServer();

	}
	protected void gotoSreenSlide() {
		// TODO Auto-generated method stub
		Intent i=new Intent(this, SlidePageActivity.class);
		startActivity(i);
	}
	
	
	

	private void showToast(String json) {
		// TODO Auto-generated method stub
		Toast.makeText(this, json, Toast.LENGTH_SHORT).show();
	}
	private void getCurrentDate() {
		// TODO Auto-generated method stub
		cal = Calendar.getInstance();
        mYear = cal.get(Calendar.YEAR);
        mMonth = cal.get(Calendar.MONTH)+1;
        mDay = cal.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat df=null;
        df=new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault());
		String strDate=df.format(cal.getTime());
		String newString=strDate.replaceAll("/", "-");
		tvDate.setText(newString);
	}
	protected void showDatePicker(View v) {
		// TODO Auto-generated method stub
		
         mYear = cal.get(Calendar.YEAR);
         mMonth = cal.get(Calendar.MONTH);
         mDay = cal.get(Calendar.DAY_OF_MONTH);

         // Launch Date Picker Dialog
         DatePickerDialog dpd = new DatePickerDialog(this,
                 new DatePickerDialog.OnDateSetListener() {
        	 		
                     @Override
                     public void onDateSet(DatePicker view, int year,
                             int monthOfYear, int dayOfMonth) {
                         // Display Selected date in textbox
                         cal.set(year, monthOfYear, dayOfMonth);
                         SimpleDateFormat df=null;
                         df=new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault());
                		 String strDate=df.format(cal.getTime());
                		 String newString=strDate.replaceAll("/", "-");
                		 tvDate.setText(newString);
                		
                     }
                     
                 }, mYear, mMonth, mDay);
         

         dpd.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {
				// TODO Auto-generated method stub
				if(cal.compareTo(Calendar.getInstance())>=0){
					showToast("Hãy Chọn Ngày Trước Thời Thời Điểm Hiện Tại");
				}
				else {
					showGiaVang();
					
				}
			}
		});
         dpd.show();
         
         
	
	}
	
	protected void showGiaVang() {
		// TODO Auto-generated method stub
		Intent i=new Intent(this, SlidePageActivity.class);
		Bundle b=new Bundle();
		b.putSerializable("DAY",cal);
		i.putExtra("DATE", b);
		startActivity(i);
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
