package com.postdatatoserver; 
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A fragment representing a single step in a wizard. The fragment shows a dummy title indicating
 * the page number, along with some dummy text.
 *
 * <p>This class is used by the {@link CardFlipActivity} and {@link
 * ScreenSlideActivity} samples.</p>
 */
public class ScreenSlidePageFragment extends Fragment {
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private String url="http://sentienich.aviostore.com/server_tienich/giavang/giavang.php";
	private static final String TAG_ID="id";
	private static final String TAG_MUA="mua";
	private static final String	TAG_BAN="ban";
	private static final String	TAG_THANHPHO="thanhpho";
	private static final String TAG_LOAIVANG="loaivang";
    private int mPageNumber;
    private ListView lv;
    private TextView tvDate;
    private Button btFinish;
    private static ArrayList<GiaVang>arrGiaVang=new ArrayList<GiaVang>();
    private String Date;
	private static Calendar cal;
	private static GiaVangAdapter vAdapter=null;
	
    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     * @param string 
     */
    public static ScreenSlidePageFragment create(int pageNumber,Date_GiaVang arrDateGiaVang, String day) {
        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
        Bundle args = new Bundle();
        args.putSerializable("GIAVANG", arrDateGiaVang.getArrGiaVang());
        args.putString("DAY", day);
        fragment.setArguments(args);
       
        return fragment;
        
    }
    
   

	public ScreenSlidePageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
        
        
        //getGiaVang();
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	// TODO Auto-generated method stub
    	super.onActivityCreated(savedInstanceState);
//    	String d= getDateFormat(cal);
    	 
    	
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout containing a title and body text.
        ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.fragment_screen_slide_page, container, false);
       arrGiaVang=(ArrayList<GiaVang>) getArguments().getSerializable("GIAVANG");
       Date=getArguments().getString("DAY");
        // Set the title view to show the page number.
       lv=(ListView)rootView.findViewById(R.id.listView1);
       vAdapter=new GiaVangAdapter(getActivity(), R.layout.listview_custom, arrGiaVang);
       vAdapter.notifyDataSetChanged();
       lv.setAdapter(vAdapter);
       tvDate=(TextView)rootView.findViewById(R.id.tvDate);
       tvDate.setText(Date);
       btFinish=(Button)rootView.findViewById(R.id.dialogButtonOK);
       btFinish.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			gotoMain();
		}

		private void gotoMain() {
			// TODO Auto-generated method stub
			Intent i =new Intent(getActivity(), MainActivity.class);
			startActivity(i);
		}
	});
        return rootView;
    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }
}
