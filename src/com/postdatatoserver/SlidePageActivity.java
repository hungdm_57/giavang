package com.postdatatoserver;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class SlidePageActivity extends FragmentActivity {
	private String url="http://sentienich.aviostore.com/server_tienich/giavang/giavang.php";
	private static final String TAG_ID="id";
	private static final String TAG_MUA="mua";
	private static final String	TAG_BAN="ban";
	private static final String	TAG_THANHPHO="thanhpho";
	private static final String TAG_LOAIVANG="loaivang";
	private ViewPager mPager;
	private static  int NUM_PAGES = 9;
	private PagerAdapter mPagerAdapter,mPagerAdapter2;
	public String day;
	
	
	Button btFinish;
	
	ProgressDialog pDialog;
	private Calendar cal;
	String d;
	Date inputDate;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_slide_page);
		if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		Intent i=getIntent();
		Bundle b=i.getBundleExtra("DATE");
		cal=(Calendar) b.getSerializable("DAY");
	
		new LoadData().execute();
		mPager=(ViewPager)findViewById(R.id.pager);
		
	}
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.activity_screen_slide, menu);

        menu.findItem(R.id.action_previous).setEnabled(mPager.getCurrentItem() > 0);

        // Add either a "next" or "finish" button to the action bar, depending on which page
        // is currently selected.
        MenuItem item = menu.add(Menu.NONE, R.id.action_next, Menu.NONE,
                (mPager.getCurrentItem() == mPagerAdapter.getCount() - 1)
                        ? R.string.action_finish
                        : R.string.action_next);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return true;
    }

	 @Override
	public boolean onOptionsItemSelected(MenuItem item) {
	        switch (item.getItemId()) {
	            case android.R.id.home:
	                // Navigate "up" the demo structure to the launchpad activity.
	                // See http://developer.android.com/design/patterns/navigation.html for more.
	                NavUtils.navigateUpTo(this, new Intent(this, MainActivity.class));
	                return true;

	            case R.id.action_previous:
	                // Go to the previous step in the wizard. If there is no previous step,
	                // setCurrentItem will do nothing.
	                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
	                return true;

	            case R.id.action_next:
	                // Advance to the next step in the wizard. If there is no next step, setCurrentItem
	                // will do nothing.
	                mPager.setCurrentItem(mPager.getCurrentItem() + 1);
	                return true;
	        }

	        return super.onOptionsItemSelected(item);
	    }
	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
		private ArrayList<String> arrayDate=new ArrayList<String>();
        private ArrayList<Date_GiaVang>arrDateGiaVang=new ArrayList<Date_GiaVang>();
        public ScreenSlidePagerAdapter(FragmentManager fm,ArrayList<Date_GiaVang>arrDateGiaVang,ArrayList<String>arrDate) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.arrayDate=arrDate;
			this.arrDateGiaVang=arrDateGiaVang;
		}

		
		

        @Override
        public Fragment getItem(int position) {
            return ScreenSlidePageFragment.create(position,arrDateGiaVang.get(position),arrayDate.get(position));
          
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
	protected ArrayList<GiaVang> getGiaVang(String day) {
		// TODO Auto-generated method stub
		List<NameValuePair> params=new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("date", day));
		GiaVang g=null;
		ServiceHandler serviceClient=new ServiceHandler();
		ArrayList<GiaVang>arr=new ArrayList<GiaVang>();
		JSONArray json=serviceClient.makeServiceCall(url, ServiceHandler.GET, params);
		if(json==null){
			//showToast("Không có dữ liệu");
		}
		else{
			try{
				for(int i=0;i<json.length();i++){

					JSONObject obj=json.getJSONObject(i);
					String id = obj.getString(TAG_ID);
					String mua=obj.getString(TAG_MUA);
					String ban=obj.getString(TAG_BAN);
					String loaiVang=obj.getString(TAG_LOAIVANG);
					String TP=obj.getString(TAG_THANHPHO);
					g=new GiaVang(id, TP, loaiVang, mua, ban);
					arr.add(g);
				}
			}catch (JSONException e){
				e.printStackTrace();
			}
		}
		return arr;
	}
	
	public void showToast(String day2) {
		// TODO Auto-generated method stub
		Toast.makeText(SlidePageActivity.this,day2, Toast.LENGTH_SHORT).show();
	}
	public String getDateFormat(Calendar cal){
		SimpleDateFormat df=null;
        df=new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault());
		String strDate=df.format(cal.getTime());
		String newString=strDate.replaceAll("/", "-");
		return newString;
	}
	private class LoadData extends AsyncTask<String, String, Calendar>{
		ArrayList<Date_GiaVang> arrDateGiaVang=new ArrayList<Date_GiaVang>();
		Date_GiaVang Dgv;
		int index;
		Calendar ca=Calendar.getInstance();
		ArrayList<GiaVang>arrGiaVang=new ArrayList<GiaVang>();
		ArrayList<String>arrayDate=new ArrayList<String>();
		GiaVang g=null;
		private ProgressDialog pDialog;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			//
			pDialog = new ProgressDialog(SlidePageActivity.this);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
		}
		@Override
		protected Calendar doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			return cal;
		}
		@Override
		protected void onPostExecute(Calendar result) {
			// TODO Auto-generated method stub
			//cal.setTime(inputDate);
			
			
			ca.setTime(result.getTime());
			getNextDataForView(ca);
			ca.setTime(result.getTime());
			getPriviousDataForView(ca);
			 try {
			        if ((this.pDialog != null) && this.pDialog.isShowing()) {
			            this.pDialog.dismiss();
			        }
			    } catch (final IllegalArgumentException e) {
			        // Handle or log or ignore
			    } catch (final Exception e) {
			        // Handle or log or ignore
			    } finally {
			        this.pDialog = null;
			    }  
			mPagerAdapter=new ScreenSlidePagerAdapter(getSupportFragmentManager(),arrDateGiaVang,arrayDate);
			mPager.setAdapter(mPagerAdapter);
			mPager.setCurrentItem(index);
			mPager.setOnPageChangeListener(new OnPageChangeListener() {
				
				@Override
				public void onPageSelected(int arg0) {
					// TODO Auto-generated method stub
					if(arg0==0||arg0==arrDateGiaVang.size()-1){
						d=arrayDate.get(arg0);
						DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
						try {
							inputDate = dateFormat.parse(d);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Calendar c=Calendar.getInstance();
						c.add(Calendar.DATE,-1);
						if(inputDate.before(c.getTime()))
						{
							cal.setTime(inputDate);
							new LoadData().execute();
						}
						
					}
				}
				
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onPageScrollStateChanged(int arg0) {
					// TODO Auto-generated method stub
					
				}
			});
			//mPager.removeAllViews();
			
			
		}
		private void getPriviousDataForView(Calendar c){
			ArrayList<String>array=new ArrayList<String>();
			ArrayList<Date_GiaVang>arrDGV=new ArrayList<Date_GiaVang>();
			for(int i=0;i<9-arrDateGiaVang.size();i++){
				c.add(Calendar.DATE, -1);
				String day=getDateFormat(c);
				ArrayList<GiaVang>arr=getGiaVang(day);
				Dgv=new Date_GiaVang(arr);
				arrDGV.add(Dgv);
				array.add(day);
			}
			index=arrDGV.size();
			Collections.reverse(arrDGV);
			arrDGV.addAll(arrDateGiaVang);
			Collections.reverse(array);
			array.addAll(arrayDate);
			arrayDate.clear();
			arrDateGiaVang.clear();
			arrayDate=array;
			arrDateGiaVang=arrDGV;
			//showToast(index+"");
		}
		private void getNextDataForView(Calendar c) {
			// TODO Auto-generated method stub
			
			while(c.getTime().before(Calendar.getInstance().getTime())&&arrDateGiaVang.size()<5){
				String day=getDateFormat(c);
				ArrayList<GiaVang>arr=getGiaVang(day);
				Dgv=new Date_GiaVang(arr);	
				arrDateGiaVang.add(Dgv);
				arrayDate.add(day);
				c.add(Calendar.DATE, +1);
			}
		}
		public void showToast(String s) {
			// TODO Auto-generated method stub
			Toast.makeText(SlidePageActivity.this, s, Toast.LENGTH_SHORT).show();
	}
	}
	
	
}	
